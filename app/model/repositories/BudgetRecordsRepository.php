<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

class BudgetRecordsRepository extends BaseDateTimeRepository
{
    /**
     * Finds budget records of a budget
     *
     * @param $budgetId
     * @return \Nette\Database\Table\Selection
     */
    public function findByBudgetId($budgetId)
    {
        return $this->findAll()->where('budget_id', $budgetId);
    }

    /**
     * Finds budget records by a phase (phase from list of all phases (table 'phases'))
     *
     * @param $phaseId
     * @return \Nette\Database\Table\Selection
     */
    public function findByPhaseId($phaseId)
    {
        return $this->findAll()->where('phase_id', $phaseId);
    }

    /**
     * Finds budget records of a project phase (phase that is assigned to project (table 'project_phases'))
     *
     * @param $projectPhaseId
     * @return \Nette\Database\Table\Selection
     */
    public function findByProjectPhaseId($projectPhaseId)
    {
        return $this->findAll()->where('project_phase_id', $projectPhaseId);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'budget_records';
    }
}