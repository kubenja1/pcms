<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

class ProjectPhasesRepository extends BaseDateTimeRepository
{
    /**
     * Finds project phases of a project
     *
     * @param $projectId
     * @return \Nette\Database\Table\Selection
     */
    public function findByProjectId($projectId)
    {
        return $this->findAll()->where('project_id', $projectId);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'project_phases';
    }
}