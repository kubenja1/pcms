<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

class HoursRepository extends BaseDateTimeRepository
{
    /**
     * Finds hours of a user
     *
     * @param $userId
     * @return \Nette\Database\Table\Selection
     */
    public function findByUserId($userId)
    {
        return $this->findAll()->where('user_id', $userId);
    }

    /**
     * Finds hours records of a project phase
     *
     * @param $projectPhaseId
     * @return \Nette\Database\Table\Selection
     */
    public function findByProjectPhaseId($projectPhaseId)
    {
        return $this->findAll()->where('project_phase_id', $projectPhaseId);
    }

    /**
     * Finds hours by an activity
     *
     * @param $activityId
     * @return \Nette\Database\Table\Selection
     */
    public function findByActivityId($activityId)
    {
        return $this->findAll()->where('activity_id', $activityId);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'hours';
    }
}