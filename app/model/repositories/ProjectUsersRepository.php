<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

class ProjectUsersRepository extends BaseDateTimeRepository
{
    /**
     * Finds assignments of projects assigned to user
     *
     * @param $userId
     * @return \Nette\Database\Table\Selection
     */
    public function findByUserId($userId)
    {
        return $this->findAll()->where('user_id', $userId);
    }

    /**
     * Finds assignments of users assigned to project
     *
     * @param $projectId
     * @return \Nette\Database\Table\Selection
     */
    public function findByProjectId($projectId)
    {
        return $this->findAll()->where('project_id', $projectId);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'project_users';
    }
}