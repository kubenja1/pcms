<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

/**
 * Base class of repositories supporting created, updated and deleted date.
 * Records are by default filtered by deleted column (can be changed in function 'getFilteredColumnName')
 *
 * Class BaseDateTimeRepository
 * @package App\Model\Repositories
 */
abstract class BaseDateTimeRepository extends BaseRepository
{
    /**
     * Inserts new row and sets created_at to current datetime
     *
     * @param array $data
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function insert(array $data)
    {
        $data[$this->getCreatedAtColumnName()] = (new \DateTime())->format('Y-m-d H:i:s');

        if (isset($data[$this->getCreatedAtColumnName()]))
            unset($data[$this->getCreatedAtColumnName()]);

        if (isset($data[$this->getDeletedAtColumnName()]))
            unset($data[$this->getDeletedAtColumnName()]);

        return parent::insert($data);
    }

    /**
     * Updates row and sets updated_at to current datetime
     *
     * @param $id
     * @param array $data
     * @return null
     */
    public function update($id, array $data)
    {
        if ($this->findById($id) === FALSE) {
            return NULL;
        }

        if (isset($data[$this->getCreatedAtColumnName()]))
            unset($data[$this->getCreatedAtColumnName()]);

        $data[$this->getUpdatedAtColumnName()] = (new \DateTime())->format('Y-m-d H:i:s');

        if (isset($data[$this->getDeletedAtColumnName()]))
            unset($data[$this->getDeletedAtColumnName()]);

        return parent::update($id, $data);
    }

    /**
     * Deletes row by setting deleted_at to current datetime. The row is then filtered by findAll()
     *
     * @param $ids
     * @return int|null
     */
    public function delete($ids)
    {
        if ($this->findByIds($ids)->count() == 0) {
            return NULL;
        }

        return $this->findByIds($ids)
            ->update([$this->getDeletedAtColumnName() => (new \DateTime())->format('Y-m-d H:i:s')]);
    }

    /**
     * Finds all filtered rows
     *
     * @return \Nette\Database\Table\Selection
     */
    public function findAll()
    {
        return $this->getTable()
            ->where($this->getFilteredColumnName(), NULL);
    }

    /**
     * Returns name of 'deleted_at' column
     *
     * @return string
     */
    protected function getDeletedAtColumnName()
    {
        return 'deleted_at';
    }

    /**
     * Returns name of 'updated_at' column
     *
     * @return string
     */
    protected function getUpdatedAtColumnName()
    {
        return 'updated_at';
    }

    /**
     * Returns name of 'created_at' column
     *
     * @return string
     */
    protected function getCreatedAtColumnName()
    {
        return 'created_at';
    }

    /**
     * Returns name of filtered date column
     *
     * @return string
     */
    protected function getFilteredColumnName()
    {
        return $this->getDeletedAtColumnName();
    }
}