<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

class BudgetsRepository extends BaseDateTimeRepository
{
    /**
     * Finds budgets of a project
     *
     * @param $projectId
     * @return \Nette\Database\Table\Selection
     */
    public function findByProjectId($projectId)
    {
        return $this->findAll()->where('project_id', $projectId);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'budgets';
    }
}