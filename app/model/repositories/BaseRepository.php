<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

use Nette\Object;

/**
 * Base class of repositories supporting standard CRUD operations
 *
 * Class BaseRepository
 * @package App\Model\Repositories
 */
abstract class BaseRepository extends Object
{
    /** @var \Nette\Database\Context */
    protected $_database = NULL;

    public function __construct(\Nette\Database\Context $database)
    {
        $this->_database = $database;
    }

    /**
     * Inserts new row
     *
     * @param array $data
     * @return bool|int|\Nette\Database\Table\IRow
     */
    public function insert(array $data)
    {
        return $this->getTable()->insert($data);
    }

    /**
     * Updates row
     *
     * @param $id
     * @param array $data
     * @return null
     */
    public function update($id, array $data)
    {
        if ($this->findById($id) === FALSE) {
            return NULL;
        }

        return $this->findById($id)->update($data);
    }

    /**
     * Deletes row
     *
     * @param $ids
     * @return int|null
     */
    public function delete($ids)
    {
        if ($this->findByIds($ids)->count() == 0) {
            return NULL;
        }

        return $this->findByIds($ids)->delete();
    }

    /**
     * Finds all rows
     *
     * @return \Nette\Database\Table\Selection
     */
    public function findAll()
    {
        return $this->getTable();
    }

    /**
     * Finds row by id
     *
     * @param $id
     * @return \Nette\Database\Table\IRow
     */
    public function findById($id)
    {
        return $this->findAll()->get($id);
    }

    /**
     * Finds rows by multiple ids
     *
     * @param $ids
     * @return \Nette\Database\Table\Selection
     */
    public function findByIds($ids)
    {
        return $this->findAll()->where('id', $ids);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected abstract function getTableName();

    /**
     * Gets a table from database by name with default table from getTableName()
     *
     * @param null $name
     * @return \Nette\Database\Table\Selection
     */
    protected function getTable($name = NULL)
    {
        return $this->_database->table($name !== NULL ? $name : $this->getTableName());
    }
}