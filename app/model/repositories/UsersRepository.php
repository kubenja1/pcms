<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;


use InvalidArgumentException;
use Nette\Security\Passwords;

class UsersRepository extends BaseDateTimeRepository
{
    /**
     * Inserts new user and hashes his password
     *
     * @param array $data
     * @return bool|int|\Nette\Database\Table\IRow|void
     */
    public function insert(array $data)
    {
        if (!isset($data['username']))
            throw new InvalidArgumentException('Username must be specified');

        $email = $data['username'];

        if ($this->findByUsername($email))
            throw new InvalidArgumentException('Username is already in use');

        $data['password'] = Passwords::hash($data['password']);

        return parent::insert($data);
    }

    /**
     * Updates user's data and hashes his password if it's meant to be updated
     *
     * @param $id
     * @param array $data
     * @return null|void
     */
    public function update($id, array $data)
    {
        if(isset($data['username'])) {
            $username = $data['username'];

            $user = $this->findByUsername($username);

            if ($user && $user['id'] != $id)
                throw new InvalidArgumentException('Username is already in use');
        }

        if (isset($data['password']))
            $data['password'] = Passwords::hash($data['password']);

        return parent::update($id, $data);
    }

    /**
     * Finds user by username
     *
     * @param $username
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function findByUsername($username)
    {
        return $this->findAll()->where('username', $username)->fetch();
    }

    /**
     * Finds users by their role
     *
     * @param $roleId
     * @return \Nette\Database\Table\Selection
     */
    public function findByRoleId($roleId)
    {
        return $this->findAll()->where('role_id', $roleId);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'users';
    }
}