<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

class ActivitiesRepository extends BaseDateTimeRepository
{
    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'activities';
    }
}