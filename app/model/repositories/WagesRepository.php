<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;


class WagesRepository extends BaseDateTimeRepository
{
    /**
     * Finds wages of a user
     *
     * @param $userId
     * @return \Nette\Database\Table\Selection
     */
    public function findByUserId($userId)
    {
        return $this->findAll()->where('user_id', $userId);
    }

    /**
     * Finds actual wage of a user
     *
     * @param $userId
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function findActualWageByUserId($userId)
    {
        return $this->findByUserId($userId)
            ->order('created_at DESC')
            ->limit(1)
            ->fetch();
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'wages';
    }
}