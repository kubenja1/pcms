<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Repositories;

class ThirdPartyExpensesRepository extends BaseDateTimeRepository
{
    /**
     * Finds expenses of a third party
     *
     * @param $thirdPartyId
     * @return \Nette\Database\Table\Selection
     */
    public function findByThirdPartyId($thirdPartyId)
    {
        return $this->findAll()->where('third_party_id', $thirdPartyId);
    }

    /**
     * Finds expenses of a project
     *
     * @param $projectPhaseId
     * @return \Nette\Database\Table\Selection
     */
    public function findByProjectPhaseId($projectPhaseId)
    {
        return $this->findAll()->where('project_phase_id', $projectPhaseId);
    }

    /**
     * Gets the name of the table it's working with
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'third_party_expenses';
    }
}