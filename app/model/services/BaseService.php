<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Services;

use App\Model\Repositories\ActivitiesRepository;
use App\Model\Repositories\BudgetRecordsRepository;
use App\Model\Repositories\BudgetsRepository;
use App\Model\Repositories\ContactsRepository;
use App\Model\Repositories\HoursRepository;
use App\Model\Repositories\PhasesRepository;
use App\Model\Repositories\ProjectPhasesRepository;
use App\Model\Repositories\ProjectsRepository;
use App\Model\Repositories\ProjectUsersRepository;
use App\Model\Repositories\ThirdPartiesRepository;
use App\Model\Repositories\ThirdPartyExpensesRepository;
use App\Model\Repositories\UsersRepository;
use App\Model\Repositories\WagesRepository;
use Nette\Object;

abstract class BaseService extends Object
{
    /** @var ActivitiesRepository */
    protected $_activitiesRepository;
    /** @var BudgetRecordsRepository */
    protected $_budgetRecordsRepository;
    /** @var BudgetsRepository */
    protected $_budgetsRepository;
    /** @var ContactsRepository */
    protected $_contactsRepository;
    /** @var HoursRepository */
    protected $_hoursRepository;
    /** @var PhasesRepository */
    protected $_phasesRepository;
    /** @var ProjectPhasesRepository */
    protected $_projectPhasesRepository;
    /** @var ProjectsRepository */
    protected $_projectsRepository;
    /** @var ProjectUsersRepository */
    protected $_projectUsersRepository;
    /** @var ThirdPartiesRepository */
    protected $_thirdPartiesRepository;
    /** @var ThirdPartyExpensesRepository */
    protected $_thirdPartyExpensesRepository;
    /** @var UsersRepository */
    protected $_usersRepository;
    /** @var WagesRepository */
    protected $_wagesRepository;

    public function injectRepositories(
        ActivitiesRepository $activitiesRepository,
        BudgetRecordsRepository $budgetRecordsRepository,
        BudgetsRepository $budgetsRepository,
        ContactsRepository $contactsRepository,
        HoursRepository $hoursRepository,
        PhasesRepository $phasesRepository,
        ProjectPhasesRepository $projectPhasesRepository,
        ProjectsRepository $projectsRepository,
        ProjectUsersRepository $projectUsersRepository,
        ThirdPartiesRepository $thirdPartiesRepository,
        ThirdPartyExpensesRepository $thirdPartyExpensesRepository,
        UsersRepository $usersRepository,
        WagesRepository $wagesRepository)
    {
        $this->_activitiesRepository = $activitiesRepository;
        $this->_budgetRecordsRepository = $budgetRecordsRepository;
        $this->_budgetsRepository = $budgetsRepository;
        $this->_contactsRepository = $contactsRepository;
        $this->_hoursRepository = $hoursRepository;
        $this->_phasesRepository = $phasesRepository;
        $this->_projectPhasesRepository = $projectPhasesRepository;
        $this->_projectsRepository = $projectsRepository;
        $this->_projectUsersRepository = $projectUsersRepository;
        $this->_thirdPartiesRepository = $thirdPartiesRepository;
        $this->_thirdPartyExpensesRepository = $thirdPartyExpensesRepository;
        $this->_usersRepository = $usersRepository;
        $this->_wagesRepository = $wagesRepository;
    }
}