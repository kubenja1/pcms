<?php
/**
 * @author Jan Kubena <kubenja1@fit.cvut.cz>
 */

namespace App\Model\Enums;

/**
 * Enum representing table 'roles'
 *
 * Class Roles
 * @package App\Model\Enums
 */
class Roles
{
    const
        Administrator   = 1,
        User            = 2;

    private static $_backArray = array(
        1 => 'Administrator',
        2 => 'User',
    );

    /**
     * Finds all roles
     */
    public static function findAll()
    {
        return Roles::$_backArray;
    }

    /**
     * Finds name of role
     *
     * @param $id
     * @return mixed
     */
    public static function findById($id)
    {
        return Roles::findAll()[$id];
    }
}